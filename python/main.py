# This is a sample Python script.
# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
import requests
import pprint
import pandas as pd
import json

# Press the green button in the gutter to run the script.
from pandas import DataFrame


#--------------------------
# Metodos que llaman al api
#--------------------------

def buscarMovie(tmdbId:int)->str:
    url= 'https://api.themoviedb.org/3/movie/'+str(tmdbId)+'?api_key=f9312051edb733bda71c27e87c02b892'
    r = requests.get(url)
    #print(r.status_code)
    # if r.ok:
    #     print(index)
    # else:
    #     dic = {'index': index,
    #             'movieID': movieID,
    #             'tmdbId': tmdbId}

    return r.json()

def buscarCast(tmdbId:int)->str:
    url= 'https://api.themoviedb.org/3/movie/'+str(tmdbId)+'/credits?api_key=f9312051edb733bda71c27e87c02b892'
    r = requests.get(url)
    #print(r.status_code)
    return r.json()

def buscarStreamingPlataform(tmdbId:int)->str:
    url= 'https://api.themoviedb.org/3/movie/'+str(tmdbId)+'/watch/providers?api_key=f9312051edb733bda71c27e87c02b892'
    r = requests.get(url)
    #print(r.status_code)
    return r.json()

def buscarUpcomingMovies()->str:
    url= 'https://api.themoviedb.org/3/movie/upcoming?api_key=f9312051edb733bda71c27e87c02b892'
    r = requests.get(url)
    #print(r.status_code)
    return r.json()




#--------------------------
# Methods to complete tables
#--------------------------

#llena la matriz de generos
def llenarGenre(genre_set: DataFrame, genres_movieID_set: DataFrame, genres_DB : DataFrame, movies_set : DataFrame)->DataFrame :


    genre_setNew = genres_movieID_set

    for index, row in genre_set.iterrows():

        movieID = row['movieId']

        genreDict = genre_set.loc[genre_set['movieId'] == movieID, 'genres']

        print(movieID)

        genres = str(genreDict.get(index))

        print(genres)


        allGenres = genres.split('|')
        #genre_setNew = genre_setNew.append(pd.Series(0, index=genre_setNew.columns), ignore_index=True)
        #genre_setNew.at[index, 'movieId'] = movieID

        for split in allGenres:

            GeneroEnGenreDB = genres_DB.loc[genres_DB['name'] == split, 'idGenre']


            #print(split)

            dict = {'movieId': str(movieID),
                    'idGenre': str(BuscarEnTablaExistenteElID(GeneroEnGenreDB))}

            genre_setNew = genre_setNew.append(dict, ignore_index=True)


    return genre_setNew

#llena la matriz de cast
def llenar_Cast(cast_set: DataFrame, movies_set: DataFrame)->DataFrame :
    indexCastID=0
    for index, row in movies_set.iterrows():

        if index>=0:
            jsonMvoieCast = buscarCast(row['tmdbId'])

            num =0
            for jsonActor in jsonMvoieCast['cast']:
                num = num+1
                #print(jsonActor)
                name = jsonActor['name']
                nameinDF = cast_set[cast_set['name']==name]

                if nameinDF.empty:
                    profilePath = 'https://www.themoviedb.org/t/p/w138_and_h175_face' + str(jsonActor["profile_path"])
                    dict = {'idCast': indexCastID,
                            'name': str(jsonActor['name']),
                            'characterName': str(name),
                            'profilePath': str(profilePath),
                            'popularity': float(jsonActor['popularity'])}
                    cast_set = cast_set.append(dict, ignore_index=True)
                    indexCastID= indexCastID+1

                if num ==3:
                    break

    return cast_set

#llena la matriz de directores
def llenar_Director(Director_set: DataFrame, movies_set: DataFrame)->DataFrame :
    indexDirectorID=0
    for index, row in movies_set.iterrows():
        jsonMvoieCast = buscarCast(row['tmdbId'])
        for jsonDirector in jsonMvoieCast['crew']:
            if jsonDirector['job']=='Director':
                name = jsonDirector['name']
                nameinDF = Director_set[Director_set['name']==name]

                if nameinDF.empty:
                    profilePath = 'https://www.themoviedb.org/t/p/w138_and_h175_face' + str(jsonDirector["profile_path"])
                    dict = {'idDirector': indexDirectorID,
                            'name': str(jsonDirector['name']),
                            'profilePath': str(profilePath),}
                    Director_set = Director_set.append(dict, ignore_index=True)
                    indexDirectorID= indexDirectorID+1
                    break

    return Director_set

# Para buscar los diferentes streaming platforma que esxixten
def llenar_StreamingPlataformColombia(sp_set: DataFrame, movies_set: DataFrame)->DataFrame :
    indexSpID=0
    for index, row in movies_set.iterrows():

        jsonMvoieCast = buscarStreamingPlataform(row['tmdbId'])

        jsonpaises = jsonMvoieCast['results']

        try:
            jsonColombia = jsonpaises['CO']
            jsonfiltrar = jsonColombia['flatrate']
        except KeyError:
            print("ERORRE")

        ##pprint.pprint(jsonfiltrar)

        for jsonSp in jsonfiltrar:

            name = jsonSp['provider_name']
            print(name)
            nameinDF = sp_set[sp_set['providerName'] == name]

            if nameinDF.empty:
                profilePath = 'https://www.themoviedb.org/t/p/original/' + str(jsonSp["logo_path"])
                dict = {'idSp': indexSpID,
                        'providerName': str(jsonSp['provider_name']),
                        'logoPath': str(profilePath), }
                sp_set = sp_set.append(dict, ignore_index=True)
                indexSpID = indexSpID + 1

    return sp_set




# Para buscar los diferentes streaming platforma que esxixten
def llenar_StreamingPlataformMundial(sp_set: DataFrame, movies_set: DataFrame)->DataFrame :
    indexSpID=0
    for index, row in movies_set.iterrows():

        jsonMvoieCast = buscarStreamingPlataform(row['tmdbId'])

        jsonpaises = jsonMvoieCast['results']

        for jsonCountry in jsonpaises:

            jsonColombia = jsonpaises[jsonCountry]
            jsonfiltrar = jsonColombia['flatrate']

            ##pprint.pprint(jsonfiltrar)

            for jsonSp in jsonfiltrar:

                name = jsonSp['provider_name']
                print(name)
                nameinDF = sp_set[sp_set['providerName'] == name]

                if nameinDF.empty:
                    profilePath = 'https://www.themoviedb.org/t/p/original/' + str(jsonSp["logo_path"])
                    dict = {'idSp': indexSpID,
                            'providerName': str(jsonSp['provider_name']),
                            'logoPath': str(profilePath), }
                    sp_set = sp_set.append(dict, ignore_index=True)
                    indexSpID = indexSpID + 1

    return sp_set

#Eliminar las peliculas no econtradas
def EliminarPeliculas(movies_set: DataFrame,movieEliminate: DataFrame)->DataFrame :
    newMovies = movies_set
    for index, row in movieEliminate.iterrows():
        movieID = row['movieID']
        indexNames = movies_set[movies_set['movieId'] == movieID].index
        newMovies.drop(indexNames, inplace=True)
    return newMovies


# cast vs movies = 0-10899
# Director vs movies = 0- 4051
#Streaming Plataform = 0- 21
def CreateMatrix (numeroColumnas: int,typeID:str):
    data = {typeID: [],0:[]}
    df = pd.DataFrame(data)
    print(df.head())
    for i in range(1,numeroColumnas+1):
        df.insert(i+1,i,0)
    return df

#Para buscar el ID de una tabla
def BuscarEnTablaExistenteElID (df:dict)->str:
    id_Cast_To_Add = ""
    for key in df.keys():
        id_Cast_To_Add = df[key]

    return id_Cast_To_Add


# numero de peliculas 9612
def llenarPeliculas(movies_set: DataFrame, pmovies_Cast_set: DataFrame, pmovies_Director_set: DataFrame, pmovies_SP_set: DataFrame,cast_set: DataFrame,director_set: DataFrame,sP_set: DataFrame):

    newMovies = movies_set
    movies_Cast_set= pmovies_Cast_set
    movies_Director_set =pmovies_Director_set
    movies_SP_set = pmovies_SP_set


    print(newMovies.head())
    print(movies_Cast_set.head())
    print(movies_Director_set.head())
    print(movies_SP_set.head())

    for index, row in movies_set.iterrows():

        print("index " + str(index))

        movieID = row['movieId']
        #Primero agrego la pelicula a la mvoies DB

        """
        jsonMvoie = buscarMovie(row['tmdbId'])

        newMovies.loc[newMovies['movieId'] == movieID, 'adult'] = bool(jsonMvoie["adult"])
        newMovies.loc[newMovies['movieId'] == movieID, 'ratingScore'] = float(jsonMvoie["vote_average"])
        baselink ='https://www.themoviedb.org/t/p/w600_and_h900_bestv2/'+str(jsonMvoie["poster_path"])
        newMovies.loc[newMovies['movieId'] == movieID, 'posterComplete'] = baselink
        newMovies.loc[newMovies['movieId'] == movieID, 'runTime'] = int(jsonMvoie["runtime"])
        newMovies.loc[newMovies['movieId'] == movieID, 'releaseDate'] = jsonMvoie["release_date"]
        newMovies.loc[newMovies['movieId'] == movieID, 'overview'] = jsonMvoie["overview"]

        budget= int(jsonMvoie["budget"])
        if budget==0:
            newMovies.loc[newMovies['movieId'] == movieID, 'budget'] = (int(jsonMvoie["revenue"])*0.8)
        else:
            newMovies.loc[newMovies['movieId'] == movieID, 'budget'] = jsonMvoie["budget"]

        """

        # SEGUNDO agrego el cast relacionado a la movie en la que me encuentro
        jsonMvoieCast = buscarCast(row['tmdbId'])
        num = 0
        #movies_Cast_set = movies_Cast_set.append(pd.Series(0, index=movies_Cast_set.columns),ignore_index=True)
        #movies_Cast_set.at[index, 'movieId'] = movieID

        for jsonActor in jsonMvoieCast['cast']:
            num = num + 1
            name = jsonActor['name']

            #buscar el actor para saber el ID
            id_Cast_To_Add_Dict = cast_set.loc[cast_set['name'] == name, "idCast"]

            #llenar la matriz con los id identificados

            movies_Cast_set.loc[len(movies_Cast_set.index)] = [movieID, str(BuscarEnTablaExistenteElID(id_Cast_To_Add_Dict))]

            if num == 3:
                break


        # TERCERO agrego el DIRECTOR relacionado a la movie en la que me encuentro

        for jsonDirector in jsonMvoieCast['crew']:
            if jsonDirector['job'] == 'Director':
                name = jsonDirector['name']

                # buscar el actor para saber el ID
                id_Director_To_Add_Dict = director_set.loc[director_set['name'] == name, "idDirector"]

                # llenar la matriz con los id identificados

                movies_Director_set.loc[len(movies_Director_set.index)] = [movieID,str(BuscarEnTablaExistenteElID(id_Director_To_Add_Dict))]
                break

        # CUARTO agrego el streaming plataforms relacionado a la movie en la que me encuentro
        jsonMvoieCast = buscarStreamingPlataform(row['tmdbId'])
        jsonpaises = jsonMvoieCast['results']

        try:
            jsonColombia = jsonpaises['CO']
            jsonfiltrar = jsonColombia['flatrate']

            for jsonSp in jsonfiltrar:
                name = jsonSp['provider_name']
                # buscar el actor para saber el ID
                id_SP_To_Add_Dict = sP_set.loc[sP_set['providerName'] == name, "idSp"]

                # llenar la matriz con loRs id identificados
                movies_SP_set.loc[len(movies_SP_set.index)] = [movieID, str(BuscarEnTablaExistenteElID(id_SP_To_Add_Dict))]

        except KeyError:
            print("NO HAY SP disponibles en Colombia")

    print(newMovies.head())
    print(movies_Cast_set.head(6))
    print(movies_Director_set.head(6))
    print(movies_SP_set.head(6))

    movies_Cast_set.to_csv('dataFinal/movieId_idCast_DB.csv')
    movies_Director_set.to_csv('dataFinal/movieId_idDirector_DB.csv')
    movies_SP_set.to_csv('dataFinal/movieId_idSp_DB.csv')


# numero de peliculas 9612
def llenarCountryStreamingPlataform(movies_set: DataFrame, Country_set: DataFrame, PTemplate_set: DataFrame, PsP_set: DataFrame):

    Template_set= PTemplate_set
    sp_set= PsP_set
    indexSpID = 22

    for index, row in movies_set.iterrows():


        print("index " + str(index))
        movieID = row['movieId']
        print(index)

        # CUARTO agrego el streaming plataforms relacionado a la movie en la que me encuentro

        jsonMvoieCast = buscarStreamingPlataform(row['tmdbId'])
        jsonpaises = jsonMvoieCast['results']

        #pprint.pprint(jsonpaises)

        for jsonCountry in jsonpaises:
            try:

                #print("PRIMER: " + jsonCountry)
                # buscar que este el pais en la lista.
                idCountry = Country_set.loc[Country_set['idCountry'] == str(jsonCountry), "idCountry"]
                print("SEGUNDO: " + BuscarEnTablaExistenteElID(idCountry))

                jsonpais = jsonpaises[BuscarEnTablaExistenteElID(idCountry)]

                jsonfiltrar = jsonpais['flatrate']

                for jsonSp in jsonfiltrar:
                    name = jsonSp['provider_name']

                    nameinDF = sp_set[sp_set['providerName'] == name]

                    if nameinDF.empty:
                        profilePath = 'https://www.themoviedb.org/t/p/original/' + str(jsonSp["logo_path"])
                        dict = {'idSp': int(indexSpID),
                                'providerName': str(jsonSp['provider_name']),
                                'logoPath': str(profilePath), }
                        sp_set = sp_set.append(dict, ignore_index=True)
                        indexSpID = indexSpID + 1

                    print(name)
                    # buscar el para saber el ID
                    id_SP_To_Add_Dict = sp_set.loc[sp_set['providerName'] == name, "idSp"]

                    # llenar la matriz con loRs id identificados

                    print(BuscarEnTablaExistenteElID(id_SP_To_Add_Dict))

                    dict = {'movieId': movieID,
                            'idSP': int(BuscarEnTablaExistenteElID(id_SP_To_Add_Dict)),
                            'idCountry': str(BuscarEnTablaExistenteElID(idCountry)), }
                    Template_set = Template_set.append(dict, ignore_index=True)

            except KeyError:
                print('------------------------------------------')
                print('ERRORRR')
                print('------------------------------------------')


    print(Template_set.tail())
    print(sp_set.tail())

    sp_set.to_csv('NewData/StreamingPlataform_DB.csv', index=False)
    Template_set.to_csv('NewData/movieId_idSp_idCountry_DB.csv')

if __name__ == '__main__':


    #r = buscarMovie(12665)
    #pprint.pprint(r)

    #pprint.pprint(buscarCast(862))

    #------------------------------------------------------
    #METODO PRINCIPAL LLENAR TODAS LAS TABLAS
    #------------------------------------------------------
    #movies_set = pd.read_csv("./dataFinal/Movies_DB.csv")

    #movies_Cast_set = pd.read_csv("./data/Templates/movieId_idCast.csv")
    #movies_Director_set = pd.read_csv("./data/Templates/movieId_idDirector.csv")
    #movies_SP_set = pd.read_csv("./data/Templates/movieId_idSp.csv")

    #cast_set = pd.read_csv("./dataFinal/Cast_DB.csv")
    #director_set = pd.read_csv("./dataFinal/Director_DB.csv")
    #sP_set = pd.read_csv("./dataFinal/StreamingPlataform_DB.csv")

    #NewMoviesset = llenarPeliculas(movies_set, movies_Cast_set,movies_Director_set,movies_SP_set,cast_set,director_set,sP_set)
    #NewMoviesset.to_csv('dataFinal/Movies_DB.csv')


    #------------------------------------------------------
    #METODO para llenar los paises de las streaming plataforms
    #------------------------------------------------------
    movies_set = pd.read_csv("./dataFinal/Movies_DB.csv")
    Country_set = pd.read_csv("./NewData/Country_DB.csv")
    Template_set = pd.read_csv("./NewData/Templates/movieId_idSp_idCountry.csv")
    sP_set = pd.read_csv("./dataFinal/StreamingPlataform_DB.csv")

    #print(Country_set.head())

    llenarCountryStreamingPlataform(movies_set, Country_set,Template_set,sP_set)
    #NewMoviesset.to_csv('dataFinal/Movies_DB.csv')


    #------------------------------------------------------
    #ELIMINATE MOVIES THAT ARE NOT IN THE DATABASE OF THE API
    #------------------------------------------------------
    #movies_set = pd.read_csv("./data/genresOLD.csv")
    #moviesAEliminar = pd.read_csv("./data/PeliculasQueNoEstan.csv")
    #NewMovies = EliminarPeliculas(movies_set,moviesAEliminar)
    #NewMovies.to_csv('GenresNew.csv')


    #print(movies_set.head(5))
    #print(movies_set.count())

    #pprint.pprint(buscarMovie(11862))
    # r = requests.get('https://api.themoviedb.org/3/movie/862/credits?api_key=f9312051edb733bda71c27e87c02b892')
    #pprint.pprint(r.json())R



    #------------------------------------------------------
    #Crear tabla de generos  - posibles problemas con casillas en sin valor.
    #------------------------------------------------------
    
    #genres_set = pd.read_csv("./NewData/Templates/genres_movies_con_ID.csv")
    #genres_movieID_set = pd.read_csv("./NewData/Templates/movieId_IdGenre.csv")
    #genres_DB = pd.read_csv("./NewData/Genres_DB.csv")

    #final = llenarGenre(genres_set,genres_movieID_set, genres_DB, movies_set)
    #final.to_csv('NewData/movieId_IdGenre_DB.csv')


    #------------------------------------------------------
    # Crear tabla de cast con todos los actores
    #------------------------------------------------------
    #cast_set = pd.read_csv("./data/CastDB.csv")
    #final_cast = llenar_Cast(cast_set,movies_set)
    #final_cast.to_csv('dataFinal/Cast_DB.csv')

    #------------------------------------------------------
    # Crear tabla de Director con todos los directores
    #------------------------------------------------------
    #Director_set = pd.read_csv("./data/DirectorDB.csv")
    #final_Directo = llenar_Director(Director_set,movies_set)
    #final_Directo.to_csv('dataFinal/Director_DB.csv')

    #------------------------------------------------------
    # Crear tabla de SP con todos los SP
    #------------------------------------------------------
    #sp_set = pd.read_csv("./data/StreamingPlataformDB.csv")
    #final_sp = llenar_StreamingPlataform(sp_set,movies_set)
    #final_sp.to_csv('dataFinal/StreamingPlataform_DB.csv')


    #------------------------------------------------------
    # Crear tabla de SP con todos los MUNDIAL
    #------------------------------------------------------
    #sp_set = pd.read_csv("./NewData/Templates/StreamingPlataformDB.csv")
    #final_sp = llenarCountryStreamingPlataform(sp_set,movies_set)
    #final_sp.to_csv('dataFinal/StreamingPlataform_DB.csv')




