-- Create Person table


CREATE TABLE Country(
   idCountry VARCHAR(2) NOT NULL PRIMARY KEY
  ,name      VARCHAR(44) NOT NULL
);

CREATE TABLE Genre(
   idGenre INTEGER  NOT NULL PRIMARY KEY 
  ,name    VARCHAR(16) NOT NULL
);


CREATE TABLE movieIdIdSpIdCountryDB(
   ID        INTEGER  NOT NULL PRIMARY KEY 
  ,movieId   INTEGER  NOT NULL
  ,idSP      INTEGER  NOT NULL
  ,idCountry VARCHAR(2) NOT NULL
);

CREATE TABLE MoviedGenreDB(
   ID      INTEGER  NOT NULL PRIMARY KEY 
  ,movieId INTEGER  NOT NULL
  ,idGenre INTEGER 
);






