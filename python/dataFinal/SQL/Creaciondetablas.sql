-- Create Person table


CREATE TABLE Movie(
   movieId        INTEGER  NOT NULL PRIMARY KEY 
  ,title          VARCHAR(94)
  ,tmdbId         INTEGER  NOT NULL
  ,adult          BOOLEAN  NOT NULL
  ,ratingScore    FLOAT NOT NULL
  ,posterComplete VARCHAR(94) NOT NULL
  ,runTime        INTEGER  NOT NULL
  ,releaseDate    DATE  NOT NULL
  ,overview       VARCHAR(1200)
  ,budget         INT  NOT NULL
  ,numLikes       INTEGER NOT NULL
);

--4052
CREATE TABLE Director(
   idDirector  INTEGER  NOT NULL PRIMARY KEY 
  ,name        VARCHAR(42) NOT NULL
  ,profilePath VARCHAR(91) NOT NULL
);


-- 10900
CREATE TABLE Casts(
   idCast        INTEGER  NOT NULL PRIMARY KEY 
  ,name          VARCHAR(41) NOT NULL
  ,characterName VARCHAR(41) NOT NULL
  ,profilePath   VARCHAR(91) NOT NULL
  ,popularity    FLOAT NOT NULL
);

-- 22
CREATE TABLE StreamingPlataform(
   idSp         INTEGER  NOT NULL PRIMARY KEY 
  ,providerName VARCHAR(28) NOT NULL
  ,logoPath     VARCHAR(82) NOT NULL
);

--9742
CREATE TABLE MoviedGenreDB(
   movieId          INTEGER  NOT NULL PRIMARY KEY 
  ,Action           BIT  NOT NULL
  ,Adventure        BIT  NOT NULL
  ,Animation        BIT  NOT NULL
  ,Children         BIT  NOT NULL
  ,Comedy           BIT  NOT NULL
  ,Crime            BIT  NOT NULL
  ,Documentary      BIT  NOT NULL
  ,Drama            BIT  NOT NULL
  ,Fantasy          BIT  NOT NULL
  ,FilmNoir         BIT  NOT NULL
  ,Horror           BIT  NOT NULL
  ,Musical_Mystery  BIT  NOT NULL
  ,Romance          BIT  NOT NULL
  ,SciFi            BIT  NOT NULL
  ,Thriller         BIT  NOT NULL
  ,War_Western      BIT  NOT NULL
  ,Mystery          BIT  NOT NULL
  ,War              BIT  NOT NULL
  ,Musical          BIT  NOT NULL
  ,IMAX             BIT  NOT NULL
  ,Western          BIT  NOT NULL
  ,no_genres_listed BIT  NOT NULL
);

--28372
CREATE TABLE movieIdIdCastDB(
   ID      INTEGER  NOT NULL PRIMARY KEY 
  ,movieId INTEGER  NOT NULL REFERENCES Movie (movieId)
  ,idCast  INTEGER  NOT NULL REFERENCES Casts (idCast)
);

--9608
CREATE TABLE movieIdIdDirectorDB(
   ID         INTEGER  NOT NULL PRIMARY KEY 
  ,movieId    INTEGER  NOT NULL REFERENCES Movie (movieId)
  ,idDirector INTEGER  NOT NULL REFERENCES Director (idDirector)
);

--5392
CREATE TABLE movieIdIdSpDB(
   ID      INTEGER  NOT NULL PRIMARY KEY 
  ,movieId INTEGER  NOT NULL REFERENCES Movie (movieId)
  ,idSP    INTEGER  NOT NULL REFERENCES StreamingPlataform (idSp)
);






