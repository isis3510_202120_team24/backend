-- Create Person table
CREATE TABLE Movie
(
    movieId INT IDENTITY PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    tmdbId INT  NOT NULL,
    adult BIT NOT NULL,
    ratingScore FLOAT NOT NULL,
    posterCompleteNOT VARCHAR(255) NULL,
    runTime INT NOT NULL,
    releaseDate DATE NOT NULL,
    overview VARCHAR(255) NOT NULL,
    budget FLOAT NOT NULL,
    numLikes INT NOT NULL

)

CREATE TABLE Director
(
    idDirector INT IDENTITY PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    profilePath VARCHAR(255) NULL
)

CREATE TABLE Cast
(
    idCast INT IDENTITY PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    characterName VARCHAR(255) NOT NULL,
    profilePath VARCHAR(255) NULL,
    popularity FLOAT NOT NULL
)

CREATE TABLE StreamingPlataform
(
    idSp INT IDENTITY PRIMARY KEY,
    providerName VARCHAR(255) NOT NULL,
    logoPath VARCHAR(255) NULL
)

CREATE TABLE MoviedGenreDB
(
    movieId INT REFERENCES Movie (movieId) PRIMARY KEY,
    Action VARCHAR(255),
    Adventure VARCHAR(255),
    Animation VARCHAR(255),
    Children VARCHAR(255),
    Comedy VARCHAR(255),
    Crime VARCHAR(255),
    Documentary VARCHAR(255),
    Drama VARCHAR(255),
    Fantasy VARCHAR(255),
    Film-Noir VARCHAR(255),
    Musical Mystery VARCHAR(255),
    Romance VARCHAR(255),
    Sci-Fi VARCHAR(255),
    Thriller VARCHAR(255),
    War Western VARCHAR(255),
    Mystery VARCHAR(255),
    War VARCHAR(255),
    Musical VARCHAR(255),
    IMAX,Western VARCHAR(255),
    (no genres listed) VARCHAR(255)
    
)



